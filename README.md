# AssoConnect – Technical interview

## Subject
We are working on the REST API of a news blog with Symfony 4 and Doctrine.

The news blog contains articles and comments with the following data:

### Article
- Title
- Body
- Publishing date

### Comment
- Author’s pseudo
- Author’s email
- Author’s phone number
- Author’s address
- Author’s sex
- Body
- Date and time

## Tips
- this project contains a minimalist install of Symfony 4, feel free to add any relevant dependencies
- the subject is not precise on purpose, focus on what is the most important
